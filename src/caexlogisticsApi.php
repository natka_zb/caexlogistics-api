<?php

use app\modules\delivery\models;
use app\modules\order\models\Order;
use app\modules\delivery\helpers\DeliveryApiHelper;

/**
 * Class caexlogisticsApi
 */
class caexlogisticsApi extends models\DeliveryApiAbstract
{
    use DeliveryApiHelper;

    const SEND_ORDER = true;

    public $params;
    public $charCode;
    public $url;
    public $client;
    public $error;

    /**
     * @param bool|\app\modules\order\models\Order $order
     * @return array
     */
    public function sendOrder($order = false)
    {
        $this->charCode = strtoupper($order->country->char_code);
        $this->getConfig();
        $this->setParams($order);
        $log = print_r($this->tccOrder,true) . PHP_EOL;
        $client = new SoapClient($this->url);

        try {
            $response = $client->GrabarDespacho4($this->tccOrder);
            if ($response->remesa == '-1') {
                throw new Exception($response->mensaje);
            }
            $resultApi = 'remesa: ' . $response->remesa . PHP_EOL;
            $resultApi.= 'mensaje: ' . $response->mensaje . PHP_EOL;
            $resultApi.= "URLRelacionEnvio : " . $response->URLRelacionEnvio . PHP_EOL;
            $resultApi.= "URLRotulos : " . $response->URLRotulos . PHP_EOL;

            $log .= $resultApi;
            $this->saveLog($log);

        }catch(Exception $e){
            $log .= $e->getMessage() . PHP_EOL . print_r($e->getTrace(), true);
            $this->saveLog($log,'send',self::ERROR);
            return models\DeliveryApiResponse::fail('Error send: ' . $e->getMessage(), 'system', $log);
        }

        $this->getLabel($order->id, $response->IMGRelacionEnvio);
        return models\DeliveryApiResponse::success([
            'tracking' => $response->remesa,
            'cs_response' => $log
        ]);
    }

    /**
     * @param bool|\app\modules\order\models\Order $order
     * @return array
     */
    public function getOrderInfo($order = false)
    {

        $tracking = $order->deliveryRequest->tracking;
        $log = "{$order->id} - {$tracking}" . PHP_EOL;

        $client = new SoapClient($this->trackingUrl);
        $this->getConfig();

        $remesas[] = ['numeroremesa' => $tracking];

        $params = [
            'Clave' => $this->config['objDespacho']['clave'],
            'remesas' => $remesas,
            'Respuesta' => 1,
        ];

        try {
            $response = $client->ConsultarInformacionRemesasEstados($params);
            if ($response->Respuesta == '-99') {
                throw new Exception($response->Mensaje);
            }
            $status = $response->remesasrespuesta->RemesaEstados->codigonovedad;
            if (!$statusId = $this->getStatus($status)) {
                throw new Exception('Unknown status code ' . $status . ' ' . $response->remesasrespuesta->RemesaEstados->Novedad);
            }
        } catch (Exception $e) {
            $log .= $e->getMessage() . PHP_EOL;
            $this->saveLog($log, 'update', self::ERROR);
            return DeliveryApiResponse::fail('Error: ' . $e->getMessage(), 'courier', $log);
        }

        return DeliveryApiResponse::success([
            'status' => $statusId,
            'foreign_status' => $status,
        ]);
    }

    /**
     * @param $deliveryStatus
     * @return int|string
     */
    protected function getStatus($deliveryStatus)
    {
        $statuses = [
            2 => 12,
            3 => 12,
            4 => 19,
            5 => 16,
            6 => 16,
            7 => 16,
            8 => 16,
            9 => 16,
            10 => 16,
            11 => 16,
            12 => 16,
            13 => 16,
            14 => 16,
            15 => 16,
            16 => 16,
            17 => 16,
            18 => 16,
            19 => 16,
            20 => 16,
            21 => 16,
            22 => 16,
            23 => 16,
            24 => 16,
            25 => 16,
            26 => 16,
            27 => 16,
            28 => 16,
            29 => 16,
            30 => 16,
            31 => 16,
            32 => 16,
            43 => 16,
            44 => 16,
            45 => 16,
            46 => 16,
            47 => 21,
            48 => 16,
            49 => 16,
            50 => 16,
            51 => 16,
            52 => 16,
            53 => 16,
            54 => 16,
            55 => 15,
            56 => 15,
            57 => 15,
            68 => 12,
            69 => 16,
            70 => 21,
            71 => 16,
            72 => 16,
            73 => 19,
            74 => 19,
            75 => 19,
            76 => 19,
            77 => 19,
            88 => 19,
            89 => 12,
            90 => 12,
            91 => 12,
            92 => 12,
            93 => 12,
            94 => 12,
            95 => 12,
            96 => 12,
            97 => 12,
            98 => 12,
            99 => 12,
            100 => 29,
            101 => 29,
            102 => 29,
            103 => 16,
            104 => 19,
            105 => 16,
            106 => 19,
            107 => 19,
            108 => 19,
            109 => 19,
            110 => 19,
            111 => 12,
            112 => 12,
            114 => 12,
            115 => 12,
            116 => 12,
            117 => 12,
            118 => 12,
            119 => 12,
            120 => 12,
            121 => 12,
            122 => 29,
            123 => 15,
            124 => 12,
            125 => 15,
            126 => 12,
            127 => 12,
            128 => 19,
            130 => 19,
            131 => 19,
            132 => 19,
            133 => 16,
            134 => 16,
            135 => 12,
            136 => 16,
            137 => 16,
            138 => 16,
            139 => 19,
            140 => 19,
            141 => 19,
            142 => 19,
            143 => 29,
            144 => 12,
            146 => 12,
            147 => 12,
            148 => 12,
            149 => 19,
            150 => 16,
            151 => 16,
            152 => 16,
            153 => 19,
        ];
        if (array_key_exists($deliveryStatus, $statuses)) {
            return $statusId = $statuses[$deliveryStatus];
        }
        return false;
    }

    /**
     * @param \app\modules\order\models\Order $order
     */
    public function setParams($order)
    {
        $this->url = $this->config['url'];
        $products = [];
        foreach ($order->orderProducts as $product) {
            $product = [
                'dicecontener' => $product->product->name,  //Product name
                'cantidadunidades' => $product->quantity, //Quantity
                'valormercancia' => $product->price
            ];
            $products[] = $this->config['unidad'] + $product;
        }

        //from config
        $objDespacho = $this->config['objDespacho'];
        $objDespacho += [
            'razonsocialdestinatario' => $order->customer_full_name, //full name
            'direcciondestinatario' => $order->customer_address,
            'telefonodestinatario' => $order->customer_phone,
            'ciudaddestinatario' => $order->customer_city_code,
            'barriodestinatario' => '',  //District not required
            'fechadespacho' => date('Y-m-d', strtotime('+ 1 day')),
            'observaciones' => $order->comment,
            //'totalvalorproducto' => $order->price_total, //total price
            'unidad'  => $products
        ];

        $this->tccOrder = [
            'objDespacho' => $objDespacho,
        ];
        $this->tccOrder += $this->config['data'];
    }

    public function getConfig()
    {
        if (isset($this->charCode) && file_exists(__DIR__ . '/' . $this->charCode . '/config.php')) {
            $this->config = require(__DIR__ . '/' . $this->charCode . '/config.php');
        } else {
            $this->config = require(__DIR__ . '/config.php');
        }
    }

    /**
     * @param $orderId
     * @param $label
     * return void
     */

    public function getLabel($orderId, $label)
    {
        $labelsDir = Yii::getAlias('@labels') . DIRECTORY_SEPARATOR . 'tcc' . DIRECTORY_SEPARATOR;
        /*$labelsDir = __DIR__ . DIRECTORY_SEPARATOR . "downloads" . DIRECTORY_SEPARATOR . "labels" .
            DIRECTORY_SEPARATOR;*/
        if (!is_dir($labelsDir)) {
            mkdir($labelsDir, 0777, true);
        }
        $fileName = $labelsDir . $orderId . '.pdf';
        file_put_contents($fileName, $label);
    }
}