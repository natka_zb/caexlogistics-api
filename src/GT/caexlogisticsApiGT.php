<?php

require_once(__DIR__ . '/../caexlogisticsApi.php');

class caexlogisticsApiGT extends caexlogisticsApi
{
    public function sendOrder($order = false)
    {
        return parent::sendOrder($order);
    }

    public function getOrderInfo($order = false)
    {
        return parent::getOrderInfo($order);
    }
}